#include "Peripheral_Setup.h"
#include "math.h"

//float R = 10;//0.05;
//float L = 0.005;
float Tsw = 50;
float meioTsw = 25;
float Kp = 1;//0.05212;//22;
float Ki = 0.01;//1.5642;//1000;
float Isat = 100;
//float i_alpha = 0;
//float i_alphant = 0;
float i_alph_ref = 0;
float i_alpha_med = 0;
//float i_beta = 0;
//float i_betant = 0;
float i_beta_ref = 0;
float i_beta_med = 0;
volatile float v_alpha = 0;
volatile float v_beta = 0;
#pragma DATA_SECTION(v_alpha,"SHARERAMGS1");
#pragma DATA_SECTION(v_beta,"SHARERAMGS1"); // allocates the variable in shared memory


//INTEGRADOR
typedef struct
{
float y;
float y_ant;
float e;
float e_ant;
float out;
}Integrador;
#define Int_default {0,0,0,0,0}
Integrador CA = Int_default;
Integrador CB = Int_default;

uint32_t index = 0, indexadc = 0;
unsigned char send = 0, turn_off_command = 0, turn_on_command = 0;
float plot1[512], plot2[512];
//float *pv_alpha =  &v_alpha;
//float *pv_beta =  &v_beta;
//float *padc1 = &i_alpha;
//float *padc2 = &i_alph_ref;

__interrupt void isr_cpu_timer0(void);
__interrupt void isr_adc(void);
__interrupt void isr_IPC1(void);

// State Machine implementation
typedef enum
{
    RUN,
    ENABLE,
    DISABLE,
    ERROR,
}init_state;

typedef enum
{
    overcurrent,
    off_state,
    on_state,
    none,
}events;

init_state ini(void)
{
    return RUN;
}

init_state end_init_goto_ON(void)
{
    EINT;
    ERTM;
    return ENABLE;
}

init_state goto_OFF(void)
{
    DINT;
    v_alpha = 0;
    v_beta = 0;
    return DISABLE;
}

init_state goto_ERROR(void)
{
    DINT;
    i_alpha_med = 0;
    i_beta_med = 0;
    v_alpha = 0;
    v_beta = 0;
    return ERROR;
}

init_state readevents(void)
{
    if(i_alpha_med > Isat || i_beta_med > Isat || i_alpha_med < -Isat || i_beta_med < -Isat){
            return overcurrent;
        }
        if(turn_off_command == 1){
            PieCtrlRegs.PIEIER1.bit.INTx7 = 0;
            return off_state;
        }
        if(turn_on_command == 1){
            PieCtrlRegs.PIEIER1.bit.INTx7 = 1;
            return on_state;
        }
        return none;
    }


int main(void){

    init_state next_state = RUN;
    events new_event;
    while(1){
        events new_event = readevents();
        switch(next_state){
        case RUN:{
            InitSysCtrl();                          // Initialize System Control:

                EALLOW;
                CpuSysRegs.PCLKCR0.bit.CPUTIMER0 = 1;
                EDIS;

                DINT;                                   // Disable CPU interrupts
                InitPieCtrl();                          // Initialize the PIE control registers to their default state
                IER = 0x0000;                           // Disable CPU interrupts
                IFR = 0x0000;                           // Clear all CPU interrupt flags:

                EALLOW;
                MemCfgRegs.GSxMSEL.bit.MSEL_GS7 = 1;
                MemCfgRegs.GSxMSEL.bit.MSEL_GS14 = 1;
                DevCfgRegs.CPUSEL14.bit.DAC_A = 1; //Transfer ownership of DACA to CPU02
                DevCfgRegs.CPUSEL14.bit.DAC_B = 1; //Transfer ownership of DACB to CPU02

                EDIS;

                InitPieVectTable();                     // Initialize the PIE vector table

                Setup_GPIO();
                Setup_ePWM();
                Setup_ADC();
                Setup_DAC();

                // waiting CPU2
                IpcRegs.IPCSET.bit.IPC5 = 1;            // Sends flag to CPU2
                while (IpcRegs.IPCSTS.bit.IPC4 == 0) ;  // Wait for CPU02 to set IPC4
                IpcRegs.IPCACK.bit.IPC4 = 1;

                EALLOW;
                PieVectTable.TIMER0_INT = &isr_cpu_timer0;
                PieVectTable.ADCA1_INT = &isr_adc;
                PieVectTable.IPC1_INT = &isr_IPC1;
                PieCtrlRegs.PIEIER1.bit.INTx7 = 1;      //Timer 0
                PieCtrlRegs.PIEIER1.bit.INTx1 = 1;      //ADC
                PieCtrlRegs.PIEIER1.bit.INTx14 = 1;     // IPC1 ISR
                EDIS;
                IER |= M_INT1;

                InitCpuTimers();
                ConfigCpuTimer(&CpuTimer0, 200, Tsw);
                CpuTimer0Regs.TCR.all = 0x4001;

                IpcRegs.IPCCLR.all = 0xFFFFFFFF;


                //EINT;                                   // Enable Global interrupt INTM
                //ERTM;                                   // Enable Global realtime interrupt DBGM
                GpioDataRegs.GPBDAT.bit.GPIO34 = 1;
                GpioDataRegs.GPADAT.bit.GPIO31 = 0;

                next_state = end_init_goto_ON();
        }
        break;
        case ENABLE:{
            if(overcurrent == new_event){
                next_state = goto_ERROR();
            }
            if(off_state == new_event){
                next_state = goto_OFF();
            }
        }
        break;
        case DISABLE:{
            if(on_state == new_event){
                next_state = end_init_goto_ON();
            }
        }
        break;
        case ERROR:{
            if(on_state == new_event){
                next_state = end_init_goto_ON();
            }
        }
        break;
        default:
            break;
        }
    }

    return 0;
}

__interrupt void isr_cpu_timer0(void){
    //GpioDataRegs.GPATOGGLE.bit.GPIO31 = 1;

    if(send == 0){
           IpcRegs.IPCSENDADDR = &v_alpha;
           IpcRegs.IPCSENDCOM = &v_beta;
           IpcRegs.IPCSET.bit.IPC2 = 1;
           send = 1;
       }

    // Control loop

    index = (index == 322) ? 0 : (index+1);
    i_alph_ref = 50*sinf(2*M_PI*60*Tsw*index*(1/1000000.0));
    i_beta_ref = 50*sinf(2*M_PI*60*Tsw*index*(1/1000000.0)+M_PI/2);

    // i_alpha
    CA.e = i_alph_ref - i_alpha_med;
    CA.y = CA.y_ant + meioTsw*(CA.e + CA.e_ant);
    CA.y_ant = CA.y;
    CA.e_ant = CA.e;
    CA.out = Kp*CA.e + Ki*CA.y;
    v_alpha = CA.out;

    // i_beta
    CB.e = i_beta_ref - i_beta_med;
    CB.y = CB.y_ant + meioTsw*(CB.e + CB.e_ant);
    CB.y_ant = CB.y;
    CB.e_ant = CB.e;
    CB.out = Kp*CB.e + Ki*CB.y;
    v_beta = CB.out;

    // Converter model
//        i_alpha = (float)(*pv_alpha-i_alphant*R)*Tsw/(L*1000000.0);
//        i_beta = (float)(*pv_beta-i_betant*R)*Tsw/(L*1000000.0);
//
//        i_alphant += i_alpha;
//        i_betant += i_beta;
//
//        EALLOW;
//        DacaRegs.DACVALS.bit.DACVALS = 3700*((Isat + i_alphant)/(2*Isat));
//        DacbRegs.DACVALS.bit.DACVALS = 3700*((Isat + i_betant)/(2*Isat));
//        EDIS;
//
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;      //clear INT1 flag
}

__interrupt void isr_adc(void){
    GpioDataRegs.GPADAT.bit.GPIO14 = 1;

    i_alpha_med = (2*Isat)*AdcaResultRegs.ADCRESULT0/3700.0 - Isat;
    i_beta_med = (2*Isat)*AdcaResultRegs.ADCRESULT1/3700.0 - Isat;

    indexadc = (indexadc == 511) ? 0 : (indexadc+1);
//    plot1[indexadc] = i_alpha_med;//*padc1; // Imedido
//    plot2[indexadc] = 50*sinf(2*M_PI*60*indexadc/(1.5*15360.0));//*padc2; /// Iref_alpha
    plot1[indexadc] = i_beta_med;//*padc2; // Imedido
    plot2[indexadc] = 50*sinf(2*M_PI*60*indexadc/(1.5*15360.0)+M_PI/2);//*padc2; /// Iref_beta

    AdcaRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;      //clear INT1 flag
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
    GpioDataRegs.GPADAT.bit.GPIO14 = 0;
}

__interrupt void isr_IPC1(void){
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;         // Must acknowledge the PIE group
    IpcRegs.IPCACK.bit.IPC1 = 1;                    // Clear IPC0 flag

}
